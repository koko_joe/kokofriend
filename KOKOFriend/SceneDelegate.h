//
//  SceneDelegate.h
//  KOKOFriend
//
//  Created by SHENG PENG LO on 2022/4/7.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

